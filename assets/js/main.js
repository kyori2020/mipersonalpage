import { darkTheme } from './components/darkTheme.js'
import { load } from './components/load.js'
import {menuResponsive} from './components/menuResponse.js'

document.addEventListener('DOMContentLoaded', function () {
    darkTheme()
    load(3000).init()
    menuResponsive()
})