export function darkTheme(){
    let themeButton = document.getElementById('switch')
    themeButton.addEventListener('click',(ev)=>{
        if(ev.target.checked){
            document.documentElement.setAttribute('tema', 'oscuro');
        }else{
            document.documentElement.setAttribute('tema', 'claro');
        }
    })

}