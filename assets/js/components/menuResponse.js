export function menuResponsive(){
    let menu=document.getElementById('menuResponsive')
    menu.style.setProperty('display','none')
    let btnResponse=document.getElementById('btnresponsive')
    btnResponse.addEventListener('click',()=>{
        menu.style.setProperty('display','block')
    })
    let btnclose=document.getElementById('close')
    btnclose.addEventListener('click',()=>{
        menu.style.setProperty('display','none')
    })
}